# Pulsating Sphere 5

ElmerFEM model for the acoustic field radiated by a pulsating sphere in free field.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Mesh Order and Accuracy](https://computational-acoustics.gitlab.io/website/posts/14-mesh-order-and-accuracy/).

## Study Summary

The main parameters of the study are reported below.

### Source and Medium

|Parameter Name              | Symbol       | Value | Unit                      |
|----------------------------|--------------|-------|---------------------------|
| Source Radius              | $`a`$        | 0.005 | meters                    |
| Source Frequency           | $`f`$        | 1000  | hertz                     |
| Source Surface Velocity    | $`U`$        | 0.75  | meters per second         |
| Medium Sound Phase Speed   | $`c_{0}`$    | 343   | meters per second         |
| Medium Equilibrium Density | $`\rho_{0}`$ | 1.205 | kilograms per cubic meter |

### Domain

| Shape     | Size              | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size | Element Order | Parallel Jobs |
|-----------|-------------------|-----------------|----------------|----------------|---------------|---------------|
| Spherical | 0.1 meters radius | NETGEN 1D-2D-3D | 1 millimetre   | 5 millimetres  | Second        | 8             |

### Boundary Condition

Matched impedance.

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        |
|----------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. The mesh is exported into `elmerfem` as `elmerfem/Mesh_1.unv`. Note that the mesh in this file is **not** computed.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/pulsating-sphere-5.git
cd pulsating-sphere-5/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

